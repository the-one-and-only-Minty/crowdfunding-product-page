const HAMBURGER = document.getElementById('hamburger');
const HAMBURGER_ITEMS = document.querySelector('.nav-list');
const BOOKMARK = document.getElementById('bookmark');

BOOKMARK.addEventListener('click', () => {
    if ( BOOKMARK.classList.contains('is-active') == false ) {
        BOOKMARK.classList.add('is-active');
    }
});

HAMBURGER.addEventListener('click', () => {
    HAMBURGER.classList.toggle('is-active');
    HAMBURGER_ITEMS.classList.toggle('is-active');
    documentOverlay();
});

let documentOverlay = () => {
    const OVERLAY_ITEM = document.createElement('div');
    const OVERLAY_CLASS = 'body-overlay';
    const OVERLAY_EL = document.querySelector(`.${OVERLAY_CLASS}`);
    OVERLAY_ITEM.classList.add(OVERLAY_CLASS);
    if ( !OVERLAY_EL ) {
        document.body.append(OVERLAY_ITEM);
        document.body.style = 'overflow: hidden;';
    } else {
        OVERLAY_EL.remove();
        document.body.style = 'overflow: visible;';
    }
}

function vh(v) {
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    return (v * h) / 100;
  }

document.getElementsByTagName('body')[0].onscroll = () => {
    var bodyRect = document.body.getBoundingClientRect(),
    elemRect = HAMBURGER.getBoundingClientRect(),
    offset   = elemRect.top - bodyRect.top;

    if( offset > vh(35) &&  HAMBURGER.classList.contains('dark') == false){
        HAMBURGER.classList.add('dark');
    }
    if( offset <= vh(35) &&  HAMBURGER.classList.contains('dark') == true){
        HAMBURGER.classList.remove('dark');
    }
};